package IF_ICICIPkg

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"strconv"

	"tk.com/util/log"

	"tk.com/encoding/json/BootConfig"
)

func (obj *Config) Init() (err error) {
	var path string
	HOME_PATH = os.Getenv("GOPATH")
	if HOME_PATH != "" {
		HOME_PATH += "/conf/"
	} else {
		HOME_PATH += "./../../conf/"
	}
	path = HOME_PATH + MODULE_NAME + ".json"
	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Println("Error", "Error", err)
		err = errors.New(path + " : file read fail")
		return
	}
	var conf BootConfig.Root
	err = json.Unmarshal(content, &conf)
	if err != nil {
		log.Println("Error", "Error", err)
		err = errors.New("Config parsing fail")
		return
	}
	err = obj.fillLoggerInfo(conf)
	if err != nil {
		log.Println("Error", "Error", err)
		err = errors.New("Logger info fill fail")
		return
	}

	err = obj.fillMqInfo(conf)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("MQ info data fill fail")
		return
	}

	err = obj.fillDbInfo(conf)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("DB info fill fail")
		return
	}

	err = obj.fillPartnerInfo(conf)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("DB info fill fail")
		return
	}

	return
}

func (obj *Config) fillMqInfo(conf BootConfig.Root) (err error) {
	obj.MqInfo = make(map[string]MQInfo)
	queues := conf.Tksp.Config.Queues
	for i := range queues {
		var mq MQInfo
		mq.Autoack, err = strconv.ParseBool(queues[i].Queue.Autoack)
		if err != nil {
			log.Println(LOG_LEVEL, "Error", err)
			err = errors.New("MQ AutoAck invalid data")
			return
		}
		mq.Binding = queues[i].Queue.Binding
		mq.Ctag = queues[i].Queue.Ctag
		mq.Durable, err = strconv.ParseBool(queues[i].Queue.Durable)
		if err != nil {
			log.Println(LOG_LEVEL, "Error", err)
			err = errors.New("MQ Durable invalid data")
			return
		}
		mq.Exchange = queues[i].Queue.Exchange
		mq.ExchangeType = queues[i].Queue.ExchangeType
		mq.ExpiryTime = queues[i].Queue.ExpiryTime
		mq.IP = queues[i].Queue.IP
		mq.Name = queues[i].Queue.Name
		mq.Nowait, err = strconv.ParseBool(queues[i].Queue.Nowait)
		if err != nil {
			log.Println(LOG_LEVEL, "Error", err)
			err = errors.New("MQ NoWait invalid data")
			return
		}
		mq.Password = queues[i].Queue.Password
		mq.Port, err = strconv.Atoi(queues[i].Queue.Port)
		if err != nil {
			log.Println(LOG_LEVEL, "Error", err)
			err = errors.New("MQ Port invalid data")
			return
		}
		mq.Username = queues[i].Queue.Username
		mq.Vhost = queues[i].Queue.Vhost
		obj.MqInfo[queues[i].Module] = mq
	}

	obj.Mq.AutoAck = obj.MqInfo[MODULE_NAME].Autoack
	obj.Mq.Ctag = obj.MqInfo[MODULE_NAME].Ctag
	obj.Mq.Durable = obj.MqInfo[MODULE_NAME].Durable
	obj.Mq.Exchange = obj.MqInfo[MODULE_NAME].Exchange
	obj.Mq.ExchangeType = obj.MqInfo[MODULE_NAME].ExchangeType
	obj.Mq.Ip = obj.MqInfo[MODULE_NAME].IP
	obj.Mq.Port = obj.MqInfo[MODULE_NAME].Port
	obj.Mq.NoWait = obj.MqInfo[MODULE_NAME].Nowait
	obj.Mq.Username = obj.MqInfo[MODULE_NAME].Username
	obj.Mq.Password = obj.MqInfo[MODULE_NAME].Password
	obj.Mq.Vhost = obj.MqInfo[MODULE_NAME].Vhost
	obj.Mq.Queue = obj.MqInfo[MODULE_NAME].Name
	obj.Mq.Key = obj.MqInfo[MODULE_NAME].Binding

	return
}

func (obj *Config) fillDbInfo(conf BootConfig.Root) (err error) {
	DBs := conf.Tksp.Config.Database
	for i := range DBs {
		obj.Db.Ip = DBs[i].IP
		obj.Db.Port = DBs[i].Port
		obj.Db.Username = DBs[i].Username
		obj.Db.Password = DBs[i].Password
		obj.Db.Type = DBs[i].Type
		obj.Db.Schema = DBs[i].Name
		break
	}
	return
}

func (obj *Config) fillLoggerInfo(conf BootConfig.Root) (err error) {
	LOG_LEVEL = conf.Tksp.Config.Logging.Level
	return
}

func (obj *Config) fillPartnerInfo(conf BootConfig.Root) (err error) {
	obj.Partner = make(map[string]Partner)
	co := conf.Tksp.Config.Partner.PartnerConfig
	for i := range co {
		var p Partner
		p.url = co[i].URL
		p.csrKey = co[i].CSRKey
		p.sslKey = co[i].SSLKey
		p.to = co[i].TimeOut
		p.request = co[i].Request
		p.uname = co[i].Username
		p.pass = co[i].Password
		p.method = co[i].Method
		p.ishttps = co[i].IsHttps
		obj.Partner[co[i].ServiceName] = p
	}
	return
}
