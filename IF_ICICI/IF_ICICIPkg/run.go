package IF_ICICIPkg

import "tk.com/util/log"

func (obj *Config) Run() (err error) {

	err = obj.listen()
	if err != nil {
		return
	}
	return
}

func (obj *Config) listen() (err error) {
	log.Println(LOG_LEVEL, "Info", "Waiting for message")
	rabbitmq := obj.Mq
	for d := range rabbitmq.DeliveriesChan {

		log.Println(LOG_LEVEL, "Debug", "Queue message header - ", d.Headers)
		log.Println(LOG_LEVEL, "Debug", "Queue message received - ", d.Body)

		go obj.handleRequest(d.Body, d.Headers)
	}
	return
}
