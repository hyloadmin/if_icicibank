package IF_ICICIPkg

import (
	"tk.com/database/sql"
	"tk.com/encoding/json/SwitchApi"
	"tk.com/net/rabbitmq"
)

type Config struct {
	MqInfo   map[string]MQInfo
	Mq       rabbitmq.RabbitMQ
	Db       sql.Database
	Error    map[string][3]string
	LogLevel string
	Partner  map[string]Partner
}

type Controller struct {
	config  Config
	message SwitchApi.Root
	header  map[string]interface{}
}

type MQInfo struct {
	Autoack      bool
	Binding      string
	Ctag         string
	Durable      bool
	Exchange     string
	ExchangeType string
	ExpiryTime   string
	IP           string
	Name         string
	Nowait       bool
	Password     string
	Port         int
	Username     string
	Vhost        string
}

var (
	HOME_PATH     = ""
	LOG_LEVEL     = ""
	PARTNER_ERROR map[string][2]string
	ERROR         map[string][3]string
	DEK           = ""
	MEK           = ""
	MDK           = ""
	RETRY_TIME    = 0
	ALLOWED_RETRY = "false"
	RETRY_QUERY   = ""
)

type Partner struct {
	url     string
	to      string
	sslKey  string
	csrKey  string
	uname   string
	pass    string
	request string
	ishttps string
	method  string
}

type CollectPayRequest struct {
	PayerVa                string `json:"payerVa,omitempty"`
	Amount                 string `json:"amount,omitempty"`
	Note                   string `json:"note,omitempty"`
	CollectByDate          string `json:"collectByDate,omitempty"`
	MerchantId             string `json:"merchantId,omitempty"`
	MerchantName           string `json:"merchantName,omitempty"`
	SubMerchantId          string `json:"subMerchantId,omitempty"`
	SubMerchantName        string `json:"subMerchantName,omitempty"`
	TerminalId             string `json:"terminalId,omitempty"`
	MerchantTranId         string `json:"merchantTranId,omitempty"`
	BillNumber             string `json:"billNumber,omitempty"`
	txnNo                  string `json:"txnNo,omitempty"`
	pConfig                Partner
	pReq                   string
	encrypted_request      string
	OnlineRefund           string `json:"onlineRefund,omitempty"`
	RefundAmount           string `json:"refundAmount,omitempty"`
	OriginalBankRRN        string `json:"originalBankRRN,omitempty"`
	OriginalmerchantTranId string `json:"originalmerchantTranId,omitempty"`
	PayeeVA                string `json:"payeeVA,omitempty"`
}

type BalanceRequest struct {
	TransactionType string
	ProcessingCode  string
	AccountNumber   string
}
type Request struct {
	txnNo             string
	ProcessingCode    string
	pConfig           Partner
	pReq              string
	encrypted_request string
	CollectPayRequest CollectPayRequest
}

type Response struct {
	TrnRefNo          string
	Status            string
	StatusDescription string
	Message           string
	Description       string
}
