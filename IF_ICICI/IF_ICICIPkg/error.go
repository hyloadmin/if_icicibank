package IF_ICICIPkg

import (
	"encoding/json"
	"strings"

	"tk.com/encoding/json/SwitchApi"
	"tk.com/util/log"

	"tk.com/util/datetime"
)

func (ctx *Controller) replyError(label string, header map[string]interface{}) {

	hCode := ""
	hMsg := ""
	log.Println(LOG_LEVEL, "Error", label)

	tmp := strings.Split(label, "{{.}}")
	if len(tmp) == 3 {
		label = tmp[0]
		hCode = tmp[1]
		hMsg = tmp[2]
	} else {
		label = tmp[0]
	}
	srcExchange := header["SourceExchange"]
	srcKey := header["SourceKey"]
	header["SourceExchange"] = ctx.config.Mq.Exchange
	header["SourceKey"] = ctx.config.Mq.Key
	header["DestinationExchange"] = srcExchange
	header["DestinationKey"] = srcKey
	header["TimeStamp"], _ = datetime.Get("", "", "Africa/Harare")
	log.Println(LOG_LEVEL, "Info", "Response Header - ", header)
	var rsp SwitchApi.Response
	er, ok := ctx.config.Error[label]
	if !ok {
		rsp.Code = "9999"
		rsp.Message = label
		rsp.Status = "PENDING"
		rsp.HostCode = hCode
		rsp.HostMessage = hMsg
	} else {
		rsp.Code = er[0]
		rsp.Message = er[1]
		rsp.Status = er[2]
		rsp.HostCode = hCode
		rsp.HostMessage = hMsg

	}

	ctx.message.Response = &rsp
	message, err := json.Marshal(&ctx.message)
	if err != nil {
		ctx.config.Mq.Send([]byte(`{ "Response": {"Code": "9998","Message": "Sorry, System is unable to send response","Status": "PENDING"}}`), header)
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Response - ", string(message))

	head, _ := json.Marshal(header)

	_, _ = ctx.config.Db.Exec(`INSERT INTO logs.system(header, message, dump_time) VALUES ($1, $2, 'now()')`, string(head), string(message))

	ctx.config.Mq.Send(message, header)
}
