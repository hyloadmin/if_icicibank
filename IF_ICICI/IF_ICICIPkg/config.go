package IF_ICICIPkg

import (
	"tk.com/database/sql"
	"tk.com/util/log"
)

func (obj *Config) Config() (err error) {

	log.Println(LOG_LEVEL, "Info", "Connecting  queue")
	err = obj.mqConfig()
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		return
	}
	log.Println(LOG_LEVEL, "Info", "Queue connected sccessfully")

	log.Println(LOG_LEVEL, "Info", "Connecting database")
	err = obj.dbConfig()
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		return
	}
	log.Println(LOG_LEVEL, "Info", "Database connected sccessfully")

	err = obj.errorConfig()
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		return
	}
	return
}

func (obj *Config) dbConfig() (err error) {
	database := &obj.Db
	log.Println(LOG_LEVEL, "Debug", "Database config : ", database)
	err = database.Connect()
	if err != nil {
		return
	}
	return
}

func (obj *Config) errorConfig() (err error) {
	database := &obj.Db
	row, err := database.Query("select label,code,message,status from lookup.error")
	if err != nil {
		return
	}
	_, data, err := sql.Scan(row)
	if err != nil {
		return
	}
	sql.Close(row)
	obj.Error = make(map[string][3]string)
	for i := range data {
		var s [3]string
		s[0] = data[i][1]
		s[1] = data[i][2]
		s[2] = data[i][3]
		obj.Error[data[i][0]] = s
	}
	log.Println(LOG_LEVEL, "Debug", "System Error Code Map : ", obj.Error)
	ERROR = obj.Error
	log.Println(LOG_LEVEL, "Debug", "System Error Code Map : ", ERROR)

	row, err = database.Query("select code,message,system_label from lookup.payee_error where payee_id=(Select id from lookup.payee where name='" + MODULE_NAME + "')")
	if err != nil {
		return
	}
	_, data, err = sql.Scan(row)
	if err != nil {
		return
	}
	sql.Close(row)
	PARTNER_ERROR = make(map[string][2]string)
	for i := range data {
		var s [2]string
		s[0] = data[i][1]
		s[1] = data[i][2]
		PARTNER_ERROR[data[i][0]] = s
	}
	log.Println(LOG_LEVEL, "Debug", "Partner Error Code Map : ", PARTNER_ERROR)

	return
}

func (obj *Config) mqConfig() (err error) {
	rabbitmq := &obj.Mq
	log.Println(LOG_LEVEL, "Debug", "RabbitMQ config : ", rabbitmq)
	err = rabbitmq.Connect()
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Queue dial successfull")
	err = rabbitmq.DeclareExchange()
	if err != nil {
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Declare echange successfull")

	err = rabbitmq.DeclareQueue()
	if err != nil {
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Declare queue successfull")

	err = rabbitmq.BindQueue()
	if err != nil {
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Queue binding successfull")

	err = rabbitmq.Consume()
	if err != nil {
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Queue is ready to consume")
	return
}
