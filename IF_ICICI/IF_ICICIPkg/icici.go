package IF_ICICIPkg

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	netEr "net"
	"net/url"
	"strconv"
	"strings"

	//	"tk.com/util/txnno"

	"tk.com/database/sql"
	"tk.com/util/log"

	"tk.com/crypto/pkcs1"

	"tk.com/encoding/json/SwitchApi"
	"tk.com/net/http"
)

func (ctx *Controller) CollectPay() (err error) {

	var t CollectPayRequest
	SD := ctx.message.ServiceDetails
	TD := ctx.message.TransactionDetails
	// CD := ctx.message.CustomerDetails
	if TD == nil {
		err = errors.New("TRANSACTION_DETAIL_NODE_NIL")
		return
	}

	if SD == nil {
		err = errors.New("SERVICE_DETAIL_NODE_NIL")
		return
	}

	//t.txnNo = TD.TransactionNumber

	amnt, err := strconv.ParseFloat(TD.Amount, 64)
	if err != nil {
		err = errors.New("INVALID_AMOUNT")
		return
	}
	t.PayerVa = TD.PayerUPIId
	t.MerchantId = TD.MerchantId
	t.SubMerchantId = TD.SubMerchantId
	t.CollectByDate = TD.DueDate
	t.BillNumber = TD.BillNumber
	t.MerchantTranId = TD.MerchantTranId
	t.Amount = TD.Amount
	t.Note = TD.Remarks
	t.CollectByDate = TD.DueDate
	t.MerchantName = TD.MerchantName
	t.SubMerchantName = TD.SubMerchantName
	t.TerminalId = TD.TerminalId
	request, _ := json.Marshal(t)

	tmp_string := strings.Replace(string(request), `"{`, "{", -1)
	tmp_string = strings.Replace(tmp_string, `}"`, "}", -1)
	tmp_string = strings.Replace(tmp_string, `\`, "", -1)
	log.Println(LOG_LEVEL, "Debug", "Request - ", string(tmp_string))

	t.Amount = fmt.Sprintf("%.2f", amnt/100)
	t.pConfig = ctx.config.Partner[ctx.header["MessageType"].(string)]
	var response map[string]string
	response, err = t.CollectPay(&ctx.config.Db, request)
	if err != nil {
		return err
	}

	var res SwitchApi.Response
	res.Code = "0"
	res.Message = "SUCCESS"
	res.Status = "APPROVED"
	res.HostCode = response["status"]
	res.HostMessage = response["message"]

	ctx.message.Response = &res
	return

}

func (t *CollectPayRequest) CollectPay(db *sql.Database, req_b []byte) (response map[string]string, err error) {
	id := ""
	res := "{}"
	res_dump := ""
	status := ""
	partner_ref_id := ""
	var data_decrypt []byte
	defer func() {
		if err != nil {
			status = err.Error()
		} else {
			status = "SUCCESS"
		}
		if id != "" {
			_, er := db.Exec(`UPDATE logs.payee SET response=$1,response_dump=$2,status=$3,payee_ref_id=$4, response_time=now() where id=$5`, res, string(data_decrypt), status, partner_ref_id, id)
			if er != nil {
				log.Println(LOG_LEVEL, "Error", err)
				err = errors.New("PARTNER_LOG_UPDATE_FAIL")
				return
			}

		}
		return
	}()

	log.Println(LOG_LEVEL, "Debug", "IF_ICICI Request : ", string(req_b))

	encrypted_req, err := pkcs1.RsaEncrypt(t.pConfig.sslKey, req_b)
	if err != nil {
		err = errors.New("ERROR IN Encrypting")
		return
	}
	encryptedText := base64.StdEncoding.EncodeToString(encrypted_req)
	log.Println(LOG_LEVEL, "Debug", "IF_ICICI Encrypted Request : ", encryptedText)

	request := fmt.Sprintf(t.pConfig.request, encryptedText)
	log.Println(LOG_LEVEL, "Debug", "Request : ", request)

	var net http.HTTP
	defer net.Close()
	net.Timeout, _ = strconv.Atoi(t.pConfig.to)
	net.Method = t.pConfig.method
	net.CharacterSet = "text/plain; charset=utf-8"
	net.Url = t.pConfig.url
	if t.pConfig.ishttps == "true" {
		net.IsHTTPS = true
		net.VerifyHost = true
		net.CertificatePath = t.pConfig.sslKey
		net.CSRKey = t.pConfig.csrKey
	} else {
		net.IsHTTPS = false
	}
	net.Header = make([][]string, 1)
	for i := range net.Header {
		net.Header[i] = make([]string, 2)
	}
	net.Header[0][0] = "Authorization"
	net.Header[0][1] = "Basic " + t.pConfig.pass
	//request_json := request

	row, err := db.Query(`INSERT INTO logs.payee(txn_no, payee_req_id,payee_ref_id, request, request_dump, response, response_dump, payee_id, request_time,status)VALUES ($1,$2,$3,$4,$5,'{}','',(SELECT id from lookup.Payee where name=$6),now(),'') RETURNING id`, t.txnNo, t.pReq, partner_ref_id, string(req_b), request, MODULE_NAME)
	_, data, err := sql.Scan(row)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("PARTNER_LOG_INSERT_FAIL")
		return
	}
	sql.Close(row)
	if len(data) <= 0 {
		err = errors.New("PARTNER_LOG_INSERT_FAIL")
		return
	}
	id = data[0][0]
	log.Println(LOG_LEVEL, "Debug", id)
	res_dump, err = net.Communicate(request)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		if urlErr, ok := err.(*url.Error); ok {
			if netOpErr, ok := urlErr.Err.(netEr.Error); ok && netOpErr.Timeout() {
				log.Println(LOG_LEVEL, "Error", "TimeOut")
				err = errors.New("PARTNER_COMMUNICATION_TIMEOUT")
			} else {
				err = errors.New("PARTNER_COMMUNICATION_FAIL")
			}
		} else if err != nil {
			err = errors.New("PARTNER_COMMUNICATION_FAIL")
		}
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Response : ", string(res_dump))
	if res_dump == "" {
		err = errors.New("PARTNER_EMPTY_RESPONSE")
		return
	}
	privatekey := t.pConfig.csrKey

	data_decrypt, err = pkcs1.Decrypt(privatekey, string(res_dump))
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)

		err = errors.New("Request Parsing Fail")
		return
	}

	log.Println(LOG_LEVEL, "Debug", "Response : Decrypted Response ", string(data_decrypt))

	err = json.Unmarshal(data_decrypt, &response)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("PARTNER_MESSAGE_DECODING_FAIL")
		return
	}

	log.Println(LOG_LEVEL, "Debug", "Response Map : ", response)
	res = string(data_decrypt)

	partner_ref_id = response["BankRRN"]
	res_code, ok := response["response"]
	if !ok {
		err = errors.New("PARTNER_RESPONSE_CODE_NODE_NIL")
		return
	}
	rd, ok := response["message"]
	if !ok {
		err = errors.New("PARTNER_RESPONSE_MESSAGE_NODE_NIL")
		return
	}
	if res_code != "92" {
		lab, ok := PARTNER_ERROR[res_code]
		if !ok {
			err = errors.New("PARTNER_INVALID_RESPONSE_CODE_FOUND" + "{{.}}" + res_code + "{{.}}" + rd)
			return
		}
		err = errors.New(lab[1] + "{{.}}" + res_code + "{{.}}" + rd)
	}
	return
}

func (ctx *Controller) TransactionStatus() (err error) {
	var t CollectPayRequest
	SD := ctx.message.ServiceDetails
	TD := ctx.message.TransactionDetails
	// CD := ctx.message.CustomerDetails
	if TD == nil {
		err = errors.New("TRANSACTION_DETAIL_NODE_NIL")
		return
	}

	if SD == nil {
		err = errors.New("SERVICE_DETAIL_NODE_NIL")
		return
	}

	t.PayerVa = TD.PayerUPIId
	t.MerchantId = TD.MerchantId
	t.SubMerchantId = TD.SubMerchantId
	t.CollectByDate = TD.DueDate
	t.BillNumber = TD.BillNumber
	t.MerchantTranId = TD.MerchantTranId
	t.Note = TD.Remarks
	t.CollectByDate = TD.DueDate
	t.MerchantName = TD.MerchantName
	t.SubMerchantName = TD.SubMerchantName
	t.TerminalId = TD.TerminalId
	request, _ := json.Marshal(t)

	tmp_string := strings.Replace(string(request), `"{`, "{", -1)
	tmp_string = strings.Replace(tmp_string, `}"`, "}", -1)
	tmp_string = strings.Replace(tmp_string, `\`, "", -1)

	log.Println(LOG_LEVEL, "Debug", "Request - ", string(tmp_string))

	t.pConfig = ctx.config.Partner[ctx.header["MessageType"].(string)]
	var response map[string]string
	response, err = t.TransactionStatus(&ctx.config.Db, request)
	if err != nil {
		return err
	}

	var res SwitchApi.Response
	res.Code = "0"
	res.Message = "SUCCESS"
	res.Status = "APPROVED"
	res.HostCode = response["status"]
	res.HostMessage = response["message"]

	ctx.message.Response = &res
	return

}

func (t *CollectPayRequest) TransactionStatus(db *sql.Database, req_b []byte) (response map[string]string, err error) {
	id := ""
	res := "{}"
	res_dump := ""
	status := ""
	partner_ref_id := ""
	var data_decrypt []byte

	defer func() {
		if err != nil {
			status = err.Error()
		} else {
			status = "SUCCESS"
		}
		if id != "" {
			_, er := db.Exec(`UPDATE logs.payee SET response=$1,response_dump=$2,status=$3,payee_ref_id=$4, response_time=now() where id=$5`, res, string(data_decrypt), status, partner_ref_id, id)
			if er != nil {
				log.Println(LOG_LEVEL, "Error", err)
				err = errors.New("PARTNER_LOG_UPDATE_FAIL")
				return
			}

		}
		return
	}()
	log.Println(LOG_LEVEL, "Debug", "Request - ", string(req_b))

	encrypted_req, err := pkcs1.RsaEncrypt(t.pConfig.sslKey, req_b)
	if err != nil {
		err = errors.New("ERROR IN Encrypting")
		return
	}
	encryptedText := base64.StdEncoding.EncodeToString(encrypted_req)
	log.Println(LOG_LEVEL, "Debug", "IF_ICICI TransactionStatus Request : ", encryptedText)

	request := fmt.Sprintf(t.pConfig.request, encryptedText)
	log.Println(LOG_LEVEL, "Debug", " Request : ", request)

	var net http.HTTP
	defer net.Close()
	net.Timeout, _ = strconv.Atoi(t.pConfig.to)
	net.Method = t.pConfig.method
	net.CharacterSet = "text/plain; charset=utf-8"
	net.Url = t.pConfig.url
	if t.pConfig.ishttps == "true" {
		net.IsHTTPS = true
		net.VerifyHost = true
		net.CertificatePath = t.pConfig.sslKey
		net.CSRKey = t.pConfig.csrKey
	} else {
		net.IsHTTPS = false
	}
	net.Header = make([][]string, 1)
	for i := range net.Header {
		net.Header[i] = make([]string, 2)
	}
	net.Header[0][0] = "Authorization"
	net.Header[0][1] = "Basic " + t.pConfig.pass
	log.Println(LOG_LEVEL, "Debug", "IF_Telone Config : ", net)
	//request_json := request

	row, err := db.Query(`INSERT INTO logs.payee(txn_no, payee_req_id,payee_ref_id, request, request_dump, response, response_dump, payee_id, request_time,status)VALUES ($1,$2,$3,$4,$5,'{}','',(SELECT id from lookup.Payee where name=$6),now(),'') RETURNING id`, t.txnNo, t.pReq, partner_ref_id, string(req_b), request, MODULE_NAME)
	_, data, err := sql.Scan(row)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("PARTNER_LOG_INSERT_FAIL")
		return
	}
	sql.Close(row)
	if len(data) <= 0 {
		err = errors.New("PARTNER_LOG_INSERT_FAIL")
		return
	}
	id = data[0][0]
	log.Println(LOG_LEVEL, "Debug", id)
	res_dump, err = net.Communicate(request)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		if urlErr, ok := err.(*url.Error); ok {
			if netOpErr, ok := urlErr.Err.(netEr.Error); ok && netOpErr.Timeout() {
				log.Println(LOG_LEVEL, "Error", "TimeOut")
				err = errors.New("PARTNER_COMMUNICATION_TIMEOUT")
			} else {
				err = errors.New("PARTNER_COMMUNICATION_FAIL")
			}
		} else if err != nil {
			err = errors.New("PARTNER_COMMUNICATION_FAIL")
		}
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Response : ", res_dump)
	if res_dump == "" {
		err = errors.New("PARTNER_EMPTY_RESPONSE")
		return
	}
	privatekey := t.pConfig.csrKey

	data_decrypt, err = pkcs1.Decrypt(privatekey, string(res_dump))
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)

		err = errors.New("Request Parsing Fail")
		return
	}

	log.Println(LOG_LEVEL, "Debug", "Response : Decrypted Response ", string(data_decrypt))

	err = json.Unmarshal(data_decrypt, &response)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("PARTNER_MESSAGE_DECODING_FAIL")
		return
	}

	log.Println(LOG_LEVEL, "Debug", "Response Map : ", response)
	res = string(data_decrypt)

	partner_ref_id = response["BankRRN"]
	res_code, ok := response["response"]
	if !ok {
		err = errors.New("PARTNER_RESPONSE_CODE_NODE_NIL")
		return
	}
	rd, ok := response["message"]
	if !ok {
		err = errors.New("PARTNER_RESPONSE_MESSAGE_NODE_NIL")
		return
	}
	if res_code != "92" {
		lab, ok := PARTNER_ERROR[res_code]
		if !ok {
			err = errors.New("PARTNER_INVALID_RESPONSE_CODE_FOUND" + "{{.}}" + res_code + "{{.}}" + rd)
			return
		}
		err = errors.New(lab[1] + "{{.}}" + res_code + "{{.}}" + rd)
	}
	return
}

func (ctx *Controller) QR() (err error) {
	var t CollectPayRequest
	SD := ctx.message.ServiceDetails
	TD := ctx.message.TransactionDetails
	if TD == nil {
		err = errors.New("TRANSACTION_DETAIL_NODE_NIL")
		return
	}

	if SD == nil {
		err = errors.New("SERVICE_DETAIL_NODE_NIL")
		return
	}

	amnt, err := strconv.ParseFloat(TD.Amount, 64)
	if err != nil {
		err = errors.New("INVALID_AMOUNT")
		return
	}
	t.PayerVa = TD.PayerUPIId
	t.MerchantId = TD.MerchantId
	t.SubMerchantId = TD.SubMerchantId
	t.CollectByDate = TD.DueDate
	t.BillNumber = TD.BillNumber
	t.MerchantTranId = TD.MerchantTranId
	t.Amount = TD.Amount
	t.Note = TD.Remarks
	t.CollectByDate = TD.DueDate
	t.MerchantName = TD.MerchantName
	t.SubMerchantName = TD.SubMerchantName
	t.TerminalId = TD.TerminalId
	request, _ := json.Marshal(t)

	tmp_string := strings.Replace(string(request), `"{`, "{", -1)
	tmp_string = strings.Replace(tmp_string, `}"`, "}", -1)
	tmp_string = strings.Replace(tmp_string, `\`, "", -1)
	log.Println(LOG_LEVEL, "Debug", "Request : ", request)

	t.Amount = fmt.Sprintf("%.2f", amnt/100)
	t.pConfig = ctx.config.Partner[ctx.header["MessageType"].(string)]
	var response map[string]string
	response, err = t.TransactionStatus(&ctx.config.Db, request)
	if err != nil {
		return err
	}

	var res SwitchApi.Response
	res.Code = "0"
	res.Message = "SUCCESS"
	res.Status = "APPROVED"
	res.HostCode = response["status"]
	res.HostMessage = response["message"]

	ctx.message.Response = &res
	return

}

func (t *CollectPayRequest) QR(db *sql.Database, req_b []byte) (response map[string]string, err error) {
	id := ""
	res := "{}"
	res_dump := ""
	status := ""
	partner_ref_id := ""
	var data_decrypt []byte
	defer func() {
		if err != nil {
			status = err.Error()
		} else {
			status = "SUCCESS"
		}
		if id != "" {
			_, er := db.Exec(`UPDATE logs.payee SET response=$1,response_dump=$2,status=$3,payee_ref_id=$4, response_time=now() where id=$5`, res, string(data_decrypt), status, partner_ref_id, id)
			if er != nil {
				log.Println(LOG_LEVEL, "Error", err)
				err = errors.New("PARTNER_LOG_UPDATE_FAIL")
				return
			}

		}
		return
	}()

	encrypted_req, err := pkcs1.RsaEncrypt(t.pConfig.sslKey, req_b)
	if err != nil {
		err = errors.New("ERROR IN Encrypting")
		return
	}
	encryptedText := base64.StdEncoding.EncodeToString(encrypted_req)

	log.Println(LOG_LEVEL, "Debug", "IF_ICICI QR Request : ", encryptedText)

	request := fmt.Sprintf(t.pConfig.request, encryptedText)
	log.Println(LOG_LEVEL, "Debug", "Request : ", request)

	var net http.HTTP
	defer net.Close()
	net.Timeout, _ = strconv.Atoi(t.pConfig.to)
	net.Method = t.pConfig.method
	net.CharacterSet = "text/plain; charset=utf-8"
	net.Url = t.pConfig.url
	if t.pConfig.ishttps == "true" {
		net.IsHTTPS = true
		net.VerifyHost = true
		net.CertificatePath = t.pConfig.sslKey
		net.CSRKey = t.pConfig.csrKey
	} else {
		net.IsHTTPS = false
	}
	net.Header = make([][]string, 1)
	for i := range net.Header {
		net.Header[i] = make([]string, 2)
	}
	net.Header[0][0] = "Authorization"
	net.Header[0][1] = "Basic " + t.pConfig.pass
	log.Println(LOG_LEVEL, "Debug", "IF_Telone Config : ", net)
	//request_json := request

	row, err := db.Query(`INSERT INTO logs.payee(txn_no, payee_req_id,payee_ref_id, request, request_dump, response, response_dump, payee_id, request_time,status)VALUES ($1,$2,$3,$4,$5,'{}','',(SELECT id from lookup.Payee where name=$6),now(),'') RETURNING id`, t.txnNo, t.pReq, partner_ref_id, string(req_b), request, MODULE_NAME)
	_, data, err := sql.Scan(row)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("PARTNER_LOG_INSERT_FAIL")
		return
	}
	sql.Close(row)
	if len(data) <= 0 {
		err = errors.New("PARTNER_LOG_INSERT_FAIL")
		return
	}
	id = data[0][0]
	log.Println(LOG_LEVEL, "Debug", id)
	res_dump, err = net.Communicate(request)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		if urlErr, ok := err.(*url.Error); ok {
			if netOpErr, ok := urlErr.Err.(netEr.Error); ok && netOpErr.Timeout() {
				log.Println(LOG_LEVEL, "Error", "TimeOut")
				err = errors.New("PARTNER_COMMUNICATION_TIMEOUT")
			} else {
				err = errors.New("PARTNER_COMMUNICATION_FAIL")
			}
		} else if err != nil {
			err = errors.New("PARTNER_COMMUNICATION_FAIL")
		}
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Response : ", res_dump)
	if res_dump == "" {
		err = errors.New("PARTNER_EMPTY_RESPONSE")
		return
	}

	privatekey := t.pConfig.csrKey

	data_decrypt, err = pkcs1.Decrypt(privatekey, string(res_dump))
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)

		err = errors.New("Request Parsing Fail")
		return
	}

	log.Println(LOG_LEVEL, "Debug", "Response : Decrypted Response ", string(data_decrypt))

	err = json.Unmarshal(data_decrypt, &response)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("PARTNER_MESSAGE_DECODING_FAIL")
		return
	}

	log.Println(LOG_LEVEL, "Debug", "Response Map : ", response)
	res = string(data_decrypt)

	partner_ref_id = response["BankRRN"]
	res_code, ok := response["response"]
	if !ok {
		err = errors.New("PARTNER_RESPONSE_CODE_NODE_NIL")
		return
	}
	rd, ok := response["message"]
	if !ok {
		err = errors.New("PARTNER_RESPONSE_MESSAGE_NODE_NIL")
		return
	}
	fmt.Println("res_code", res_code, "rd", rd)
	if res_code != "92" {
		lab, ok := PARTNER_ERROR[res_code]
		if !ok {
			err = errors.New("PARTNER_INVALID_RESPONSE_CODE_FOUND" + "{{.}}" + res_code + "{{.}}" + rd)
			return
		}
		err = errors.New(lab[1] + "{{.}}" + res_code + "{{.}}" + rd)
	}
	return
}
func (ctx *Controller) Refund() (err error) {
	var t CollectPayRequest
	SD := ctx.message.ServiceDetails
	TD := ctx.message.TransactionDetails
	if TD == nil {
		err = errors.New("TRANSACTION_DETAIL_NODE_NIL")
		return
	}

	if SD == nil {
		err = errors.New("SERVICE_DETAIL_NODE_NIL")
		return
	}

	t.PayerVa = TD.PayerUPIId
	t.MerchantId = TD.MerchantId
	t.SubMerchantId = TD.SubMerchantId
	t.PayeeVA = TD.PayeeVA
	t.Note = TD.Remarks
	t.MerchantTranId = TD.MerchantTranId
	t.TerminalId = TD.TerminalId
	t.OnlineRefund = TD.OnlineRefund
	t.RefundAmount = TD.RefundAmount
	t.OriginalBankRRN = TD.OriginalBankRRN
	t.OriginalmerchantTranId = TD.OriginalmerchantTranId
	t.Note = TD.Remarks
	request, _ := json.Marshal(t)

	tmp_string := strings.Replace(string(request), `"{`, "{", -1)
	tmp_string = strings.Replace(tmp_string, `}"`, "}", -1)
	tmp_string = strings.Replace(tmp_string, `\`, "", -1)
	log.Println(LOG_LEVEL, "Debug", "Request : ", request)

	t.pConfig = ctx.config.Partner[ctx.header["MessageType"].(string)]
	var response map[string]string
	response, err = t.TransactionStatus(&ctx.config.Db, request)
	if err != nil {
		return err
	}

	var res SwitchApi.Response
	res.Code = "0"
	res.Message = "SUCCESS"
	res.Status = "APPROVED"
	res.HostCode = response["status"]
	res.HostMessage = response["message"]

	ctx.message.Response = &res
	return

}

func (t *CollectPayRequest) Refund(db *sql.Database, req_b []byte) (response map[string]string, err error) {
	id := ""
	res := "{}"
	res_dump := ""
	status := ""
	partner_ref_id := ""
	var data_decrypt []byte
	defer func() {
		if err != nil {
			status = err.Error()
		} else {
			status = "SUCCESS"
		}
		if id != "" {
			_, er := db.Exec(`UPDATE logs.payee SET response=$1,response_dump=$2,status=$3,payee_ref_id=$4, response_time=now() where id=$5`, res, string(data_decrypt), status, partner_ref_id, id)
			if er != nil {
				log.Println(LOG_LEVEL, "Error", err)
				err = errors.New("PARTNER_LOG_UPDATE_FAIL")
				return
			}

		}
		return
	}()

	encrypted_req, err := pkcs1.RsaEncrypt(t.pConfig.sslKey, req_b)
	if err != nil {
		err = errors.New("ERROR IN Encrypting")
		return
	}
	encryptedText := base64.StdEncoding.EncodeToString(encrypted_req)

	log.Println(LOG_LEVEL, "Debug", "IF_ICICI QR Request : ", encryptedText)

	request := fmt.Sprintf(t.pConfig.request, encryptedText)
	log.Println(LOG_LEVEL, "Debug", "Request : ", request)

	var net http.HTTP
	defer net.Close()
	net.Timeout, _ = strconv.Atoi(t.pConfig.to)
	net.Method = t.pConfig.method
	net.CharacterSet = "text/plain; charset=utf-8"
	net.Url = t.pConfig.url
	if t.pConfig.ishttps == "true" {
		net.IsHTTPS = true
		net.VerifyHost = true
		net.CertificatePath = t.pConfig.sslKey
		net.CSRKey = t.pConfig.csrKey
	} else {
		net.IsHTTPS = false
	}
	net.Header = make([][]string, 1)
	for i := range net.Header {
		net.Header[i] = make([]string, 2)
	}
	net.Header[0][0] = "Authorization"
	net.Header[0][1] = "Basic " + t.pConfig.pass
	log.Println(LOG_LEVEL, "Debug", "IF_Telone Config : ", net)
	//request_json := request

	row, err := db.Query(`INSERT INTO logs.payee(txn_no, payee_req_id,payee_ref_id, request, request_dump, response, response_dump, payee_id, request_time,status)VALUES ($1,$2,$3,$4,$5,'{}','',(SELECT id from lookup.Payee where name=$6),now(),'') RETURNING id`, t.txnNo, t.pReq, partner_ref_id, string(req_b), request, MODULE_NAME)
	_, data, err := sql.Scan(row)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("PARTNER_LOG_INSERT_FAIL")
		return
	}
	sql.Close(row)
	if len(data) <= 0 {
		err = errors.New("PARTNER_LOG_INSERT_FAIL")
		return
	}
	id = data[0][0]
	log.Println(LOG_LEVEL, "Debug", id)
	res_dump, err = net.Communicate(request)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		if urlErr, ok := err.(*url.Error); ok {
			if netOpErr, ok := urlErr.Err.(netEr.Error); ok && netOpErr.Timeout() {
				log.Println(LOG_LEVEL, "Error", "TimeOut")
				err = errors.New("PARTNER_COMMUNICATION_TIMEOUT")
			} else {
				err = errors.New("PARTNER_COMMUNICATION_FAIL")
			}
		} else if err != nil {
			err = errors.New("PARTNER_COMMUNICATION_FAIL")
		}
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Response : ", res_dump)
	if res_dump == "" {
		err = errors.New("PARTNER_EMPTY_RESPONSE")
		return
	}

	privatekey := t.pConfig.csrKey

	data_decrypt, err = pkcs1.Decrypt(privatekey, string(res_dump))
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)

		err = errors.New("Request Parsing Fail")
		return
	}

	log.Println(LOG_LEVEL, "Debug", "Response : Decrypted Response ", string(data_decrypt))

	err = json.Unmarshal(data_decrypt, &response)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		err = errors.New("PARTNER_MESSAGE_DECODING_FAIL")
		return
	}

	log.Println(LOG_LEVEL, "Debug", "Response Map : ", response)
	res = string(data_decrypt)

	partner_ref_id = response["BankRRN"]
	res_code, ok := response["response"]
	if !ok {
		err = errors.New("PARTNER_RESPONSE_CODE_NODE_NIL")
		return
	}
	rd, ok := response["message"]
	if !ok {
		err = errors.New("PARTNER_RESPONSE_MESSAGE_NODE_NIL")
		return
	}
	fmt.Println("res_code", res_code, "rd", rd)
	if res_code != "92" {
		lab, ok := PARTNER_ERROR[res_code]
		if !ok {
			err = errors.New("PARTNER_INVALID_RESPONSE_CODE_FOUND" + "{{.}}" + res_code + "{{.}}" + rd)
			return
		}
		err = errors.New(lab[1] + "{{.}}" + res_code + "{{.}}" + rd)
	}
	return
}
