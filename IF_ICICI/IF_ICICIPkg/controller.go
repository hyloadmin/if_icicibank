package IF_ICICIPkg

import (
	"encoding/json"
	"runtime/debug"

	"tk.com/util/log"

	"tk.com/util/datetime"
)

func (obj Config) handleRequest(request []byte, header map[string]interface{}) (err error) {
	var ctx Controller
	ctx.config = obj
	ctx.header = header
	log.Println(LOG_LEVEL, "Info", "Request Header - ", header)
	defer func() {
		if l_exception := recover(); l_exception != nil {
			stack := debug.Stack()
			log.Println(LOG_LEVEL, "Exception", string(stack))
			ctx.replyError("EXCEPTION", header)
		}
		return
	}()
	log.Println(LOG_LEVEL, "Debug", "Request - ", string(request))
	err = json.Unmarshal(request, &ctx.message)
	if err != nil {
		log.Println(LOG_LEVEL, "Error", err)
		ctx.replyError("MESSAGE_DECODING_FAIL", header)
		return
	}
	log.Println(LOG_LEVEL, "Debug", ctx.message)
	mType := header["MessageType"]
	switch mType {
	case "CollectPay":
		err = ctx.CollectPay()
	case "QR":
		err = ctx.QR()
	case "TransactionStatus":
		err = ctx.TransactionStatus()
	case "Refund":
		err = ctx.Refund()

	default:
		ctx.replyError("INVALID_MESSAGE_TYPE", header)
		return
	}

	if err != nil {
		ctx.replyError(err.Error(), header)
		return
	}

	message, err := json.Marshal(&ctx.message)
	if err != nil {
		ctx.replyError("MESSAGE_ENCODING_FAIL", header)
		return
	}
	log.Println(LOG_LEVEL, "Debug", "Response - ", string(message))

	srcExchange := header["SourceExchange"]
	srcKey := header["SourceKey"]
	header["SourceExchange"] = obj.Mq.Exchange
	header["SourceKey"] = obj.Mq.Key
	header["DestinationExchange"] = srcExchange
	header["DestinationKey"] = srcKey
	header["TimeStamp"], _ = datetime.Get("", "", "Africa/Harare")
	log.Println(LOG_LEVEL, "Info", "Response Header - ", header)

	head, _ := json.Marshal(header)

	_, _ = obj.Db.Exec(`INSERT INTO logs.system(header, message, dump_time) VALUES ($1, $2, 'now()')`, string(head), string(message))
	obj.Mq.Send(message, header)
	return
}
