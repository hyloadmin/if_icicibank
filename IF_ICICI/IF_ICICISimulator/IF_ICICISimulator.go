package main

import (
	"github.com/astaxie/beego"
	//	"tk.com/util/log"
)

type CollectPay struct {
	beego.Controller
}
type TransactionStatus struct {
	beego.Controller
}

type Refund struct {
	beego.Controller
}

func (this *CollectPay) Post() {
	this.Ctx.Output.Body([]byte(`F8nBiS8uCj8hgPbKtVs064gzmrpshLoio/48Lu8UKUSvXdJEpcY1ypRN+U8PK1awpdysvzQDorEe
dnRiGRgJAP4dQbT+kgEhTODyVhnStToFTE/+4KF/tt5TxuewdAJt7f/HVANFiE6atmKfxHk5N8e4
OOZepV+PjVv7t4ZoaEh+YVPrbaCkT25LR1ufrYmtpcdg8mw1JzUpsTg5jS4OCJPxFL93PFwm5yPW
3n8Ixu9WLQe7NxQJJanD01HFPE3EP+g+R+UnDu9SugJsuAznNb00kAeKevOCbzDo8vmOJN7hYcNV
V3JzWRZlXH6NtoERJl8BGgDXuF40aysHVTlu/sj+t1UyacXxpo3w/g4XbLcx6mkPi41w+kSJK1PH
1Bpl8kWPr+kdsqk+W1evO6ZyxMaK4ZmA+CMOY2BCa2JR13hoMO63XEN6FTdw5mFOWNcI5QawuUAd
9m1tidiWQ7nT03VtPp1gK52y2tgXEiPiheSskOLHyccWhXMCL60jgk058YeZkk9nhKXXzpBkf2dF
rOqX9iEFjx3xy4xlUezrWgnK6N8KgqFKllXMuYG1X9lfbAX893g5e2WNs57c1xwkrwNxMiiD26v0
IZey0emzhHOdpmTdYk7tTB7MtyXLDCd/rMS5TvEGqIaBOTO9CItaAK/TrsgKDpaty9M03vhReLo=`))
}

func (this *Refund) Post() {
	this.Ctx.Output.Body([]byte(`F8nBiS8uCj8hgPbKtVs064gzmrpshLoio/48Lu8UKUSvXdJEpcY1ypRN+U8PK1awpdysvzQDorEe
dnRiGRgJAP4dQbT+kgEhTODyVhnStToFTE/+4KF/tt5TxuewdAJt7f/HVANFiE6atmKfxHk5N8e4
OOZepV+PjVv7t4ZoaEh+YVPrbaCkT25LR1ufrYmtpcdg8mw1JzUpsTg5jS4OCJPxFL93PFwm5yPW
3n8Ixu9WLQe7NxQJJanD01HFPE3EP+g+R+UnDu9SugJsuAznNb00kAeKevOCbzDo8vmOJN7hYcNV
V3JzWRZlXH6NtoERJl8BGgDXuF40aysHVTlu/sj+t1UyacXxpo3w/g4XbLcx6mkPi41w+kSJK1PH
1Bpl8kWPr+kdsqk+W1evO6ZyxMaK4ZmA+CMOY2BCa2JR13hoMO63XEN6FTdw5mFOWNcI5QawuUAd
9m1tidiWQ7nT03VtPp1gK52y2tgXEiPiheSskOLHyccWhXMCL60jgk058YeZkk9nhKXXzpBkf2dF
rOqX9iEFjx3xy4xlUezrWgnK6N8KgqFKllXMuYG1X9lfbAX893g5e2WNs57c1xwkrwNxMiiD26v0
IZey0emzhHOdpmTdYk7tTB7MtyXLDCd/rMS5TvEGqIaBOTO9CItaAK/TrsgKDpaty9M03vhReLo=`))
}

func (this *TransactionStatus) Post() {
	this.Ctx.Output.Body([]byte(`F8nBiS8uCj8hgPbKtVs064gzmrpshLoio/48Lu8UKUSvXdJEpcY1ypRN+U8PK1awpdysvzQDorEe
dnRiGRgJAP4dQbT+kgEhTODyVhnStToFTE/+4KF/tt5TxuewdAJt7f/HVANFiE6atmKfxHk5N8e4
OOZepV+PjVv7t4ZoaEh+YVPrbaCkT25LR1ufrYmtpcdg8mw1JzUpsTg5jS4OCJPxFL93PFwm5yPW
3n8Ixu9WLQe7NxQJJanD01HFPE3EP+g+R+UnDu9SugJsuAznNb00kAeKevOCbzDo8vmOJN7hYcNV
V3JzWRZlXH6NtoERJl8BGgDXuF40aysHVTlu/sj+t1UyacXxpo3w/g4XbLcx6mkPi41w+kSJK1PH
1Bpl8kWPr+kdsqk+W1evO6ZyxMaK4ZmA+CMOY2BCa2JR13hoMO63XEN6FTdw5mFOWNcI5QawuUAd
9m1tidiWQ7nT03VtPp1gK52y2tgXEiPiheSskOLHyccWhXMCL60jgk058YeZkk9nhKXXzpBkf2dF
rOqX9iEFjx3xy4xlUezrWgnK6N8KgqFKllXMuYG1X9lfbAX893g5e2WNs57c1xwkrwNxMiiD26v0
IZey0emzhHOdpmTdYk7tTB7MtyXLDCd/rMS5TvEGqIaBOTO9CItaAK/TrsgKDpaty9M03vhReLo=`))
}

// func (this *CallbackResponse) Post() {
// 	this.Ctx.Output.Body([]byte(`[
//  {
// "response": "0",
// "merchantId": "",
// "subMerchantId": "",
// "terminalId": "",
// "success": "true",
// "message": "success",
// "merchantTranId": "",
// "BankRRN": ""
// }

// ]`))
// }

/*
type BalanceCheck struct {
	beego.Controller
}

func (this *BalanceCheck) Get() {
	this.Ctx.Output.Body([]byte(`[
    {
		"accountNumber": "22591828013",
		"accountBranch": "011",
		"accountName": "ACCOUNT NAME",
		"balance": "2134.68",
		"status": "00",
		"message": "Valid"
	}
]`))
}

func (this *PAYMENTS) Post() {
	this.Ctx.Output.Body([]byte(`[
   {
	"InstitutionID": "CLIENT-ID",
	"ExtReferenceNo": "BATCH_REF",
	"ProcessingCode": "100",
	"TransactionType": "AAT",
	"TransactionCode": "SAL",
	"SourceAcNo": "03320741960028",
	"Amt": "65261.63",
	"CCY": "ZWL",
	"Narrative": "",
	"DrCr": "D",
	"Response": [{
		"TrnRefNo": "0003664191940001",
		"Status": "SUCCESS",
		"StatusDescription": "Record Successfully Saved"
	}],
	"Contra": [{
		"BenAcNo": "01123195960023",
		"BenName": "Estimate Amend",
		"CnAmt": "5450.00",
		"BenBank": "COMMERCIAL BANK OF ZIMBABWE UNION",
		"BankSWIFTCode": "006101",
		"BenBranch": " UNION AVENUE",
		"RecordNo": "2",
		"Narrative": "Refunds",
		"Response": [{
			"TrnRefNo": "0003664191940002",
			"Status": "SUCCESS",
			"StatusDescription": "Record Successfully Saved "
		}]
	}]
}
]`))
}*/

func main() {

	beego.BConfig.Listen.HTTPPort = 7009
	beego.BConfig.CopyRequestBody = true

	beego.RESTRouter("/icici/collectpay/", &CollectPay{})
	beego.RESTRouter("/icici/transactionstatus/", &TransactionStatus{})
	beego.RESTRouter("/icici/refund/", &Refund{})

	//	beego.RESTRouter("http://localhost:5005/neobank/UPI_ICICI", &CallbackResponse{})

	beego.Run()
}
