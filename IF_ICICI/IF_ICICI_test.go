package main

import (
	"fmt"
	"testing"

	"tk.com/net/rabbitmq"
)

var (
	// net.CharacterSet = "text/plain; charset=utf-8"
	// net.Url = "http://localhost:8080/v1.0/SwitchApi"
	/*request = `{
		"Payer": {
			"Name": "HYLO"
		},
		"Payee": {
			"Name": "IF_ICICI"
		},
		"ServiceDetails": {
			"Name": "CollectPay",
			"Operator": "ICICI"


		},
		"TransactionDetails": {
			"RequestID": "43536453653464",
			"Amount": "100.00",
			"PayerUPIId":"testo@icici",
			"MerchantId":"400443",
			"SubMerchantId": "400443",
			"DueDate":"30/05/2020 06:30 PM",
			"BillNumber":"HyloTest",
			"MerchantTranId":"testhyl02",
			"Remarks":"collect-pay-request",
			"MerchantName":"Testmerchant",
			"SubMerchantName":"Test",
			"TerminalId":"5411"
		}

	}`*/

	/*request = `{
		"Payer": {
			"Name": "HYLO"
		},
		"Payee": {
			"Name": "IF_ICICI"
		},
		"ServiceDetails": {
			"Name": "TransactionStatus",
			"Operator": "ICICI"
		},
		"TransactionDetails": {
			"RequestID": "43536453653464",
			"RequestTime": "2016-01-11T11:22:22",
			"MerchantId":"400443",
			"SubMerchantId": "400443",
			"BillNumber":"sdf234234",
			"MerchantTranId":"testhyl02",
			"TerminalId":"5411"
		}

	}`
	*/
	request = `{
		"Payer": {
			"Name": "HYLO"
		},
		"Payee": {
			"Name": "IF_ICICI"
		},
		"ServiceDetails": {
			"Name": "QR",
			"Operator": "ICICI"
		},
		"TransactionDetails": {
			"RequestID": "43536453653464",
			"RequestTime": "2016-01-11T11:22:22",
			"Amount": "1900.00",
			"MerchantId":"400443",
			"MerchantTranId":"fhylosfs",
			"BillNumber":"013212052760",
			"TerminalId":"5411"

		}

	}`

	/*request = `{
		"Payer": {
			"Name": "HYLO"
		},
		"Payee": {
			"Name": "IF_ICICI"
		},
		"ServiceDetails": {
			"Name": "Refund",
			"Operator": "ICICI"
		},
		"TransactionDetails": {
			"RequestID": "43536453653464",
			"RequestTime": "2016-01-11T11:22:22",
			"PayeeVA":"test0@icici",
			"RefundAmount": "100.00",
			"SubMerchantId":"400443",
			"MerchantId":"400443",
			"MerchantTranId":"tetstQTFG",
			"TerminalId":"5411",
			"OriginalBankRRN":"013212052760",
			"OnlineRefund":"Y",
			"OriginalmerchantTranId":"testhylo01",
			"Remarks":"refund-request"
		}
	}`
	*/
	LOG_LEVEL = "Debug"
)

func TestBootConfig(t *testing.T) {
	var com rabbitmq.RabbitMQ
	com.Ip = "127.0.0.1"
	com.Port = 5672
	com.Username = "guest"
	com.Password = "guest"
	com.Vhost = ""
	com.Durable = false
	com.NoWait = false
	com.AutoAck = false
	com.Ctag = "tmp"
	com.Exchange = "VMEXC"
	com.ExchangeType = "direct"
	com.Queue = "VMQUEUE"
	com.Key = "VMKEY"

	header := make(map[string]interface{})
	header["MessageType"] = interface{}("QR")
	header["SourceExchange"] = interface{}(com.Exchange)
	header["SourceKey"] = interface{}(com.Key)
	header["DestinationExchange"] = interface{}("ExcIF_ICICI")
	header["DestinationKey"] = interface{}("IF_ICICI")
	header["RequestID"] = interface{}("43536453653464")
	header["mpin"] = interface{}("0897876543223456789765432345678")
	header["code"] = interface{}("829787614888")
	header["TransactionNo"] = interface{}("TXXXXXXXXXXXXX1")
	header["TimeStamp"] = interface{}("2016-01-11T11:22:22")

	err := com.Connect()
	if err != nil {
		t.Error(err)
		return
	}

	err = com.DeclareExchange()
	if err != nil {
		t.Error(err)
		return
	}

	err = com.DeclareQueue()
	if err != nil {
		t.Error(err)
		return
	}

	err = com.BindQueue()
	if err != nil {
		t.Error(err)
		return
	}

	err = com.Consume()
	if err != nil {
		t.Error(err)
		return
	}
	//	go recv(com)
	//	time.Sleep((3 * time.Second))
	err = com.Send([]byte(request), header)
	if err != nil {
		t.Error(err)
		return
	}

	//	fmt.Println("Request Sent..")
	//	time.Sleep(1000 * time.Second)
}

func recv(com rabbitmq.RabbitMQ) {
	fmt.Println(com.DeliveriesChan)
	for d := range com.DeliveriesChan {
		fmt.Println(string(d.Body))
		fmt.Println(d.Headers)
		com.Shutdown()
	}
}
